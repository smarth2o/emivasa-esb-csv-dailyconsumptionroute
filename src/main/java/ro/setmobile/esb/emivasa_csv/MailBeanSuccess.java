package ro.setmobile.esb.emivasa_csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.camel.Body;
import org.apache.log4j.Logger;

public class MailBeanSuccess{
	static Logger log=Logger.getLogger(MailBeanSuccess.class);
	
	public String setBody(@Body String body){
		String message="";
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("mail_success.txt");
		BufferedReader br=new BufferedReader(new InputStreamReader(in));
		String line;
		try {
			while ((line = br.readLine()) != null) {
				message+=line+"\n";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return message;
	}

}
