package ro.setmobile.esb.emivasa_csv;

import java.util.Date;
import java.util.Map;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.lang.time.DateFormatUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.istack.logging.Logger;

public class LogDbBean {
	Logger log= Logger.getLogger(LogDbBean.class);
	public String toSql(@Header("CamelFileName") String fileName,@Header("InsertCount") Integer count) {		
		String inputBody = fileName;
		StringBuilder logInsertQuery=new StringBuilder("insert into emivasa_log (timestamp, user_id,readings_inserted,message_payload) values (");
		Date date=new Date();
		String dateString=DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ss");
		logInsertQuery.append("'"+dateString+"',-1,"+count+",");
		logInsertQuery.append("'FileName:"+inputBody+"')");
		//log.info("Query String "+logInsertQuery.toString());
		return logInsertQuery.toString();
	}
}
