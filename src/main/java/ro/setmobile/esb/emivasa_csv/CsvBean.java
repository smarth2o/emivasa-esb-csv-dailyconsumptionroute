package ro.setmobile.esb.emivasa_csv;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",", crlf = "UNIX")
public class CsvBean {
	@DataField(pos = 1)
	String userId;

	@DataField(pos = 2)
	String smartMeterId;

	@DataField(pos = 3)
	String timestamp;

	@DataField(pos = 4)
	double consumption;

	@DataField(pos = 5)
	String um;

	@DataField(pos = 6)
	String zipcode;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSmartMeterId() {
		return smartMeterId;
	}

	public void setSmartMeterId(String smartMeterId) {
		this.smartMeterId = smartMeterId;
	}

	public String getTimestamp() {
		String[] split = timestamp.split("\\.");
		if (split != null && split.length >= 2) {
			
		}
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		String[] split = timestamp.split("\\.");
		if (split != null && split.length >= 2) {
			this.timestamp = split[0] + "\"";
		} else if (split.length == 1) {

			this.timestamp = timestamp;
		}
		if (this.timestamp != null)
			System.out.println("Timestamp: " + this.timestamp);
	}

	public double getConsumption() {
		return consumption;
	}

	public void setConsumption(double consumption) {
		this.consumption = consumption;
	}

	public String getUm() {
		return um;
	}

	public void setUm(String um) {
		this.um = um;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}
