package ro.setmobile.esb.emivasa_csv;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.util.log.Log;

import com.sun.istack.logging.Logger;

public class Md5ProcessorBean implements Processor{
	Logger log= Logger.getLogger(Md5ProcessorBean.class);
	
	@Override
	public void process(Exchange ex) throws Exception {
		String filename=ex.getIn().getHeader("CamelFileName").toString();
		//filename=filename.substring(0, filename.lastIndexOf('.'));
		filename+=".md5";
		
		
		Integer retryCounts=0;
		ex.getIn().setHeader("Stop", 0);
		
		String propFileName="application.properties";
		Properties prop=new Properties();
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		prop.load(inputStream);
		
		String md5Folder = prop.getProperty("file.md5");
		File f=new File(md5Folder+"\\"+filename);
		log.info("File name MD5 "+ md5Folder+"\\"+filename);
		
		
		String enableMd5 = prop.getProperty("enable.md5");
		String retryTimeoutString = prop.getProperty("retry.timeout");
		Integer retryTimeout;
		try{
			retryTimeout = Integer.parseInt(retryTimeoutString);
		}catch(Exception e){
			e.printStackTrace();			
			retryTimeout = 10;
		}
	
		if(enableMd5.equals("true")){		
			while(!f.exists() && retryCounts<retryTimeout){
				log.info("retryCounts "+retryCounts);
				Thread.sleep(1000);
				retryCounts++;
			}
			
			if(!f.exists()){
				ex.getIn().setHeader("Stop", 1);
			}else{
				
				String csvPath=ex.getIn().getHeader("CamelFileAbsolutePath").toString();
				File csvFile=new File(csvPath);
				String md5Hash = HashGeneratorUtils.generateMD5(csvFile);
				log.info("MD5 Hash: " + md5Hash);

				BufferedReader br=new BufferedReader(new FileReader(f));
				String md5file=br.readLine();
				if(!md5file.equalsIgnoreCase(md5Hash)){
					ex.getIn().setHeader("Stop", 1);
				}
				br.close();
				f.delete();

			}
		}
	}

}
