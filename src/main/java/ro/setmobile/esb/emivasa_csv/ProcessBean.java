package ro.setmobile.esb.emivasa_csv;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.sun.istack.logging.Logger;

public class ProcessBean implements Processor {
	static Logger log = Logger.getLogger(ProcessBean.class);

	Map<String, SmartMeter> dictionary;

	private Map<String, SmartMeter> getUsers(Connection con)
			throws SQLException {
		HashMap<String, SmartMeter> result = new HashMap<String, SmartMeter>();

		Statement s = con.createStatement();
		ResultSet rs = s
				.executeQuery("select n.user_oid,s.oid,s.smart_meter_id from neutral_user as n inner join household as h on n.household_oid=h.oid inner join smart_meter as s on h.smart_meter_oid=s.oid");
		while (rs.next()) {
			result.put(rs.getString(1),
					new SmartMeter(rs.getInt(2), rs.getString(3)));
		}
		rs.close();
		return result;

	}

	@Override
	public void process(Exchange arg0) throws Exception {
		List<CsvBean> list=new ArrayList<CsvBean>();
		if(arg0.getIn().getBody() instanceof CsvBean){
			list.add((CsvBean)(arg0.getIn().getBody()));
		}else{
			list = (List<CsvBean>) arg0.getIn().getBody();			
		}	
		Integer counter = 0;

		log.info("Input count" + list.size());
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			log.info("OK DRIVER");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			throw new IllegalStateException("Could not load JDBC driver class",
					ex);
		}

		String propFileName = "database.properties";
		Properties prop = new Properties();
		InputStream inputStream = getClass().getClassLoader()
				.getResourceAsStream(propFileName);
		prop.load(inputStream);

		String address = prop.getProperty("address");
		String database = prop.getProperty("database");
		String port = prop.getProperty("port");
		String username = prop.getProperty("username");
		String password = prop.getProperty("password");

		Connection con = null;
		Statement stm = null;
		String url = "jdbc:mysql://" + address + ":" + port + "/" + database;

		con = DriverManager.getConnection(url, username, password);

		log.info("Before getUser");
		dictionary = getUsers(con);
		log.info("Dictionary size: " + dictionary.size());

		stm = con.createStatement();

		for (int i = 0; i < list.size(); i++) {
			CsvBean bean = list.get(i);
			SmartMeter sm = dictionary.get(bean.userId);
			if (sm != null) {
				StringBuilder sb = new StringBuilder(
						"insert into meter_reading(reading_date_time,total_consumption,smart_meter_oid,total_consumption_adjusted) values(\'");
				sb.append(bean.getTimestamp());
				sb.append("\',");
				sb.append(bean.getConsumption());
				sb.append(",");
				sb.append(sm.getOid());
				sb.append(",");
				sb.append(bean.getConsumption());
				sb.append(") on duplicate key update total_consumption=");
				sb.append(bean.getConsumption());
				sb.append(",total_consumption_adjusted=");
				sb.append(bean.getConsumption());
				// log.info(sb.toString());
				stm.execute(sb.toString());
				if (sm.smartMeterId == null
						|| !sm.smartMeterId.equals(bean.smartMeterId)) {
					sb = new StringBuilder(
							"update smart_meter set smart_meter_id=\'");
					sb.append(bean.getSmartMeterId());
					sb.append("\' where oid=");
					sb.append(sm.getOid());
					log.info(sb.toString());
					stm.execute(sb.toString());
					sm.setSmartMeterId(bean.getSmartMeterId());
				}
				counter++;
			}
		}
		log.info("Insert count " + counter);
		arg0.getIn().setHeader("InsertCount", counter);

	}
}

class SmartMeter {

	Integer oid;
	String smartMeterId;

	public SmartMeter(Integer oid, String smartMeterId) {
		super();
		this.oid = oid;
		this.smartMeterId = smartMeterId;
	}

	public Integer getOid() {
		return oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public String getSmartMeterId() {
		return smartMeterId;
	}

	public void setSmartMeterId(String smartMeterId) {
		this.smartMeterId = smartMeterId;
	}

}
