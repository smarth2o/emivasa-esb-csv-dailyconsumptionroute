package ro.setmobile.esb.emivasa_csv;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import com.sun.istack.logging.Logger;

public class AggregationStrategyBean implements AggregationStrategy {
	Logger log= Logger.getLogger(AggregationStrategyBean.class);
	
	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		if (newExchange == null) {
			return oldExchange;
		}
		/*
		String oldBody = oldExchange.getIn().getBody(String.class);
		String newBody = newExchange.getIn().getBody(String.class);
		String body = newBody;
		oldExchange.getIn().setHeaders(new HashMap<String,Object>());
		oldExchange.getIn().setHeader(Exchange.HTTP_METHOD,"POST");
		oldExchange.getIn().setHeader(Exchange.CONTENT_TYPE,"application/json");
		oldExchange.getIn().setHeader("operationName","getDevicesInfo");
		
		oldExchange.getIn().setBody(body);*/
		log.info("NEW EXCHANGE 1: " + newExchange.getIn().getBody().toString());
		log.info("NEW EXCHANGE 1: " + newExchange.getIn().getHeader("CamelFileAbsolutePath").toString());
		log.info("NEW EXCHANGE 1: " + newExchange.getIn().getHeader("CamelFileAbsolutePath").toString());
		log.info("OLD EXCHANGE 1: " + oldExchange.getIn().getBody().toString());
		return oldExchange;
	}

}